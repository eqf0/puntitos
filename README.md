# Dotfiles

Configuration files for my machine. After I move away from a certain program, the configuration files usually remain here, but I don't really check if they continue to work.

## Usage

Run the deployment script from inside the repo

```sh
python deploy.py link --profiles emacs
```

Or whatever profile you want. Run `deploy.py --help` for more options and `deploy.py list` for the list of the currently available configurations.
