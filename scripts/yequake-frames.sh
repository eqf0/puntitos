#!/bin/sh
shopt -s nullglob globstar

selection=$(emacsclient -e "(mapcar 'car yequake-frames)" | tr -d "()" | tr '"' "\n" | awk NF | wofi -d)

emacsclient -n -e "(yequake-toggle \"$selection\")"
