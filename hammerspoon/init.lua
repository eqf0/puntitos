-- attempts to configure space movement
-- did not work

hs.spaces.setDefaultMCwaitTime(0.25);

local function gotoNthDesktop(n)
  local spaceId = nil;
  local spaceNames = hs.spaces.missionControlSpaceNames(false);
  for _, screenSpaces in pairs(spaceNames) do
    for currentId, name in pairs(screenSpaces) do
      if string.format("select Desktop %d", n) == name then
        spaceId = currentId;
      end
    end
  end
  if spaceId ~= nil then
    hs.spaces.gotoSpace(spaceId);
  else
    hs.spaces.closeMissionControl();
  end
end

hs.hotkey.bind({"cmd", "alt", "ctrl"}, "R",
  function()
    hs.reload();
  end
)

-- yabai integration

local function yabaiCmd(domain, args)
  local yabaiBin = "/opt/homebrew/bin/yabai";
  local commandBuilder = {yabaiBin, "--message", domain};
  for flag, value in pairs(args) do
    table.insert(commandBuilder, string.format("--%s", flag));
    table.insert(commandBuilder, value);
  end
  local command = table.concat(commandBuilder, " ");
  return command;
end

local function yabai(domain, args)
  local command = yabaiCmd(domain, args);
  return hs.execute(command);
end

local function orExec(cmd0, cmd1)
  local fullCommand = string.format("%s || %s", cmd0, cmd1);
  return hs.execute(fullCommand);
end

for index=1,5 do
  hs.hotkey.bind({"alt"}, tostring(index),
    function()
        yabai("space", {focus = index});
    end
  );
  hs.hotkey.bind({"alt", "shift"}, tostring(index),
    function()
      yabai("window", {space = index});
    end
  );
end

for direction, relativeSpace in pairs({left = "prev", right = "next"}) do
  hs.hotkey.bind({"alt"}, direction,
    function()
      yabai("space", {focus = relativeSpace});
    end
  );
end

hs.hotkey.bind({"alt"}, "j", function()
  orExec(
    yabaiCmd("window", {focus = "stack.next"}),
    yabaiCmd("window", {focus = "stack.first"})
  );
end)

hs.hotkey.bind({"alt"}, "k", function()
  orExec(
    yabaiCmd("window", {focus = "stack.prev"}),
    yabaiCmd("window", {focus = "stack.last"})
  );
end)

-- other keybindings
hs.hotkey.bind({"alt"}, "return", function()
  hs.execute("open -na Terminal");
end)

hs.notify.new({title="Hammerspoon", informativeText="Config finished loading!"}):send();
