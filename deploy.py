#!/usr/bin/env python3

from pathlib import Path, PurePath
import dot
import logging as log
import platform

dots = {}

dots['fontconfig'] = dot.create('fontconfig')

dots['gtk'] = dot.create(
    ('gtk/gtk-3.0', 'gtk-3.0')
) | dot.create(
    ('gtk/gtkrc-2.0', '.gtkrc-2.0'), target=Path.home()
)

dots['scripts'] = dot.create(
    ('scripts/passelect.sh', 'passelect'),
    ('scripts/yequake-frames.sh', 'yequake-frames'),
    ('scripts/import-gsettings.sh', 'import-gsettings'),
    target=Path(Path.home(), '.local/bin')
)

dots['gnupg'] = dot.create(
    ('gnupg/gpg-agent.conf', '.gnupg/gpg-agent.conf'),
    target=Path.home()
)

dots['foot'] = dot.create('foot')
dots['river'] = dot.create(
    'river/init', 'river/autostart.sh'
)
dots['waylock'] = dot.create('waylock')
dots['waybar'] = dot.create('waybar')
dots['wofi'] = dot.create('wofi')
dots['swaync'] = dot.create('swaync')
dots['xdg-desktop-portal-wlr'] = dot.create('xdg-desktop-portal-wlr')

dots['emacs'] = dot.create(
    ('emacs/init.el', 'init.el'),
    ('emacs/early-init.el', 'early-init.el'),
    ('emacs/lisp', 'lisp'),
    ('emacs/site-lisp', 'site-lisp'),
    target=dot.emacs_target_path('xdg')
)

dots['yabai'] = dot.create('yabai')
dots['skhd'] = dot.create('skhd')
dots['hammerspoon'] = dot.create('hammerspoon')

actions = {
    'link': dot.link,
    'show': dot.pretty_print,
    'list': dot.list,
}

def main():
    log.basicConfig(level=log.INFO, format='%(levelname)s: %(message)s')
    args = dot.parse()
    if args.action == 'list':
        log.info(f'listing all the available configurations')
        dot.list(dots)
    else:
        action = actions[args.action]
        for config in args.configs:
            if config in dots:
                log.info(f'linking {config}')
                action(dots[config])

if __name__ == '__main__':
    main()
