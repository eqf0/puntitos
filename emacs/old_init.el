;; Straight up setting the package manager
(setq straight-use-package-by-default t)
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name
	"straight/repos/straight.el/bootstrap.el"
	user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
	(url-retrieve-synchronously
	 "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
	 'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;; nicer package interface
(straight-use-package 'use-package)
(eval-when-compile
  (require 'use-package))
(setq use-package-verbose t)

(setq use-package-minimum-reported-time 0.001)

;; configurations not in any particular package
(use-package emacs
  :straight nil
  :config
  (setq-default indent-tabs-mode nil)
  (setq default-frame-alist '((undecorated . t))
	inhibit-startup-message t
	print-circle t
	scroll-conservatively most-positive-fixnum
	user-mail-address "hochata@disroot.org")
  (tooltip-mode -1)
  (tool-bar-mode -1)
  (scroll-bar-mode -1)
  (menu-bar-mode -1))

;; setting up fonts
(use-package faces
  :straight nil
  :config
  (set-face-attribute 'default nil :family "NotoMono Nerd Font" :height 110)
  (set-face-attribute
   'variable-pitch nil :family "NotoSans Nerd Font" :height 110)
  (set-face-attribute 'italic nil :slant 'italic :underline nil)
  (set-fontset-font t 'symbol "Noto Color Emoji"))

;; nice symbols
(use-package all-the-icons
  :if (display-graphic-p))

(use-package all-the-icons-completion
  :init
  (all-the-icons-completion-mode))

(use-package exec-path-from-shell
  :init
  (exec-path-from-shell-initialize))

;; I hope this works
(use-package browse-url
  :straight nil
  :commands (browse-url browse-url-at-point browse-url-of-file browse-url-of-buffer)
  :config
  (setq browse-url-browser-function 'browse-url-generic
	browse-url-generic-program "/usr/bin/xdg-open"))

;; less file cluttering
(use-package files
  :straight nil
  :init
  (make-directory (expand-file-name "tmp/auto-saves/" user-emacs-directory) t)
  :custom
  (safe-local-variable-values
   '((denote-directory . "~/Documents/bunsan/notes/")))
  (backup-directory-alist
   `(("." . ,(expand-file-name "tmp/backups/" user-emacs-directory))))
  (auto-save-list-file-prefix
   (expand-file-name "tmp/auto-saves/sessions/" user-emacs-directory))
  (auto-save-file-name-transforms
   `((".*" ,(expand-file-name "tmp/auto-saves/" user-emacs-directory) t))))

;; basic emacs commands
(use-package simple
  :straight nil
  :config
  (column-number-mode t))

;; line numbers (for code only)
(use-package display-line-numbers
  :straight nil
  :custom
  (display-line-numbers-type 'relative)
  :hook
  (prog-mode . display-line-numbers-mode))

(defun eqf0/modus-bg-fg (face bg fg)
  (set-face-attribute
   face
   nil
   :background (modus-themes-color bg)
   :foreground (modus-themes-color fg)))

(defun +subtle-whitespace ()
  (interactive)
  (if (bound-and-true-p whitespace-mode)
      (dolist (face '(whitespace-space
		      whitespace-tab
		      whitespace-newline
		      whitespace-indentation))
	(set-face-attribute
	 face
	 nil
	 :background (face-attribute 'default :background)))))

;; show whitespace in code
(use-package whitespace
  :straight nil
  :custom
  (whitespace-action '(warn-if-read-only auto-cleanup))
  (whitespace-style '(face tabs spaces trailing lines-tail newline empty indentation
			   big-indent help-newline tab-mark space-mark newline-mark))
  :hook
  (prog-mode . whitespace-mode)
  (whitespace-mode . +subtle-whitespace))

;; never forget what you did wrong
(use-package hl-todo
  :hook
  (prog-mode . hl-todo-mode))

;; vim like modeline
(use-package doom-modeline
  :init (doom-modeline-mode 1))

;; never forget how to emacs
(use-package which-key
  :defer 5
  :init (which-key-mode 1))

(defun eqf0/c-indent-complete()
  (interactive)
  (let (( p (point)))
    (c-indent-line-or-region)
    (when (= p (point))
      (call-interactively 'complete-symbol))))

(use-package cc-mode
  :straight nil
  :bind  (:map c-mode-base-map
	       ("TAB" . eqf0/c-indent-complete)))

;; complete me
(use-package corfu
  :config
  (setq tab-always-indent 'complete)
  :init
  (global-corfu-mode))

;; complete you
(use-package cape
  :defer 5
  :init
  (add-to-list 'completion-at-point-functions #'cape-file)
  (add-to-list 'completion-at-point-functions #'cape-dabbrev))

;; emacs help
(use-package help
  :straight nil
  :custom
  (help-window-select t))

;; nicer help
(use-package helpful
  :commands (helpful-function helpful-at-point helpful-key helpful-variable))

;; the nicest of them all
(use-package ghelp
  :straight (ghelp :type git :host github :repo "casouri/ghelp")
  :bind (("C-h C-f" . ghelp-describe-function)
	 ("C-h C-v" . ghelp-describe-variable)
	 ("C-h C-k" . ghelp-describe-key)
	 ("C-h C-d" . ghelp-describe-at-point)))

;; adorkable org files
(use-package mixed-pitch
  :commands mixed-pitch-mode)

;; emacs is the only browser I trust
(use-package eww
  :commands eww
  :straight nil
  :config
  (setq eww-search-prefix "https://searx.be/search?q="))

;; fill my head with garbage
(use-package visual-fill-column
  :custom
  (visual-fill-column-center-text t)
  (visual-fill-column-fringes-outside-margins nil)
  (visual-fill-column-width 85)
  :hook ((markdown-mode org-mode Info-mode eww-mode) . visual-fill-column-mode))

;; wrap my world in whitespace
(use-package visual-line-mode
  :straight nil
  :hook (markdown-mode org-mode Info-mode eww-mode))

;; purity is a requirement
(use-package haskell-mode
  :mode "\\.hs\\'"
  :custom
  (haskell-process-type 'stack-ghci)
  :hook
  (haskell-mode . interactive-haskell-mode))

(defun eqf0/modus-pdf-theme ()
  (if (eq (modus-themes--current-theme) 'modus-vivendi)
      (pdf-view-midnight-minor-mode 1)
    (pdf-view-midnight-minor-mode -1)))

;; emacs as a pdf reader
(use-package pdf-tools
  :mode "\\.pdf\\'"
  :hook (pdf-tools-enabled . eqf0/modus-pdf-theme)
  :init
  (pdf-loader-install))

;; https://emacs.stackexchange.com/questions/44664/apply-ansi-color-escape-sequences-for-org-babel-results
(defun ek/babel-ansi ()
  (when-let ((beg (org-babel-where-is-src-block-result nil nil)))
    (save-excursion
      (goto-char beg)
      (when (looking-at org-babel-result-regexp)
	(let ((end (org-babel-result-end))
	      (ansi-color-context-region nil))
	  (ansi-color-apply-on-region beg end))))))

;; plain life in plain text
(use-package org
  :mode "//.org//'"
  :hook (org-babel-after-execute . ek/babel-ansi)
  :bind (:map org-mode-map
	      ("C-c <return>" . org-insert-item))
  :custom
  (org-log-done 'time)
  (org-src-preserve-indentation t)
  (org-image-actual-width nil)
  (org-confirm-babel-evaluate nil)
  (org-export-with-toc nil)
  (org-todo-keywords
   '("TODO(t)" "NEXT(n)" "|" "WAITING(w)" "DONE(d)" "CANCELLED(c)"))
  :config
  (plist-put org-format-latex-options :scale 1.5)) ; bigger fragment size

;; org exports
(use-package ox-latex
  :straight nil
  :after org-mode
  :custom
  (org-latex-listings 'minted)
  (org-latex-compiler "xelatex")
  (org-latex-pdf-process
   '("latexmk -f -shell-escape -%latex -output-directory=%o %f"))
  :config
  (add-to-list 'org-latex-packages-alist '("" "minted")))

;; planner
(use-package org-agenda
  :straight nil
  :commands org-agenda
  :custom
  (org-agenda-files '("~/Documents/bunsan/gcal.org" "~/Documents/bs/todo.org")))

;; bring org into the present
(use-package org-modern
  :hook ((org-mode . org-modern-mode)
	 (org-mode . (lambda () (setq line-spacing 0.25)))))

(defun eqf0/markdown-to-pdf ()
  (interactive)
  (async-shell-command
   (format "/usr/bin/pandoc --from markdown --to pdf -o %s.pdf %s"
	   (shell-quote-argument (file-name-base (buffer-file-name)))
	   (shell-quote-argument (buffer-file-name)))))

(use-package ix
  :commands ix)

;; inferior org
(use-package markdown-mode
  :mode "\\.md\\'"
  :custom
  (markdown-fontify-code-blocks-natively t)
  (markdown-enable-math t)
  (markdown-header-scaling t)
  (markdown-marginalize-headers t)
  (markdown-command '("pandoc" "--from=markdown" "--to=pdf")))

(defun eqf0/project-vterm ()
  (interactive)
  (let* ((default-directory (project-root (project-current t)))
	 (vterm-buffer-name (project-prefixed-buffer-name "vterm"))
	 (vterm-buffer (get-buffer vterm-buffer-name)))
    (if (and vterm-buffer (not current-prefix-arg))
	(pop-to-buffer vterm-buffer)
      (vterm-other-window vterm-buffer-name))))

;; builtin project manager
(use-package project
  :straight nil
  :bind ((:map project-prefix-map) ("t" . eqf0/project-vterm))
  :custom
  (project-switch-commands 'project-find-file)
  :defer 5)

;; elisp's little cousing is comming to visit
(use-package sly
  :hook common-lisp-mode
  :straight nil ;; installed with roswell
  :custom
  (org-babel-lisp-eval-fn 'sly-eval)
  :config
  (load (expand-file-name "~/.roswell/helper.el"))
  (org-babel-do-load-languages
   'org-babel-load-languages
   (append org-babel-load-languages '((lisp . t)))))

;; a little reason in this post modern world
(use-package prolog
  :mode ("\\.pl\\'" . prolog-mode)
  :straight nil)

;; introduce some sense into every emacs buffer
(use-package ediprolog
  :straight (ediprolog :type git :host github :repo "triska/ediprolog")
  :commands ediprolog-dwim
  :custom
  (prolog-electric-if-then-else-flag t))

;; finally something I hate more than I hate myself
(use-package cmake-mode
  :mode "CMakeLists.txt")

;; the fastest terminal in this side of the mariana trench
(use-package vterm
  :commands vterm
  :custom
  (vterm-shell "/bin/zsh"))

;;black magic for git integration
(use-package magit
  :bind ("C-x j" . magit-status)
  :custom
  (magit-display-buffer-function
   'magit-display-buffer-traditional))

;; python at the speed of c
(use-package nim-mode
  :mode "\\.nim\\'")

;; colorful failures
(use-package ansi-color
  :config
  (defun my-colorize-compilation-buffer ()
    (when (eq major-mode 'compilation-mode)
      (ansi-color-apply-on-region compilation-filter-start (point-max))))
  :hook (compilation-filter . my-colorize-compilation-buffer))

;; emacs build system
(use-package compile
  :straight nil
  :commands (compile project-compile)
  :custom
  (compilation-scroll-output 'first-error))

;; yaaaaas (just a joke)
(use-package yasnippet
  :config
  (yas-reload-all)
  :hook
  (tex-mode . yas-minor-mode))

;; yaaaaas (for real this time)
(use-package yasnippet-snippets
  :after yasnippet)

(use-package julia-mode
  :mode "\\.jl\\'")

(use-package julia-snail
  :after vterm
  :hook (julia-mode . julia-snail-mode))

(use-package cuda-mode
  :mode "\\.cu\\'")

;; nice tsoding sessions
(use-package gruber-darker-theme
  :commands load-theme)

(use-package catppuccin-theme
  :straight (catppuccin-theme :type git :host github :repo "catppuccin/emacs")
  :commands load-theme)

(use-package emojify
  :commands emojify-mode)

;; readable json
(use-package yaml-mode
  :mode "\\.yml\\'")

;; new shinny blog
(use-package org-static-blog
  :commands org-static-blog-publish
  :custom
  (org-static-blog-posts-directory
   (expand-file-name "~/Documents/repos/blog/content/blog"))
  (org-static-blog-drafts-directory
   (expand-file-name "~/Documents/repos/blog/content/drafts")))

;; time to get productive
(use-package org-pomodoro
  :commands org-pomodoro
  :custom
  (org-pomodoro-manual-break t))

(use-package ob-shell
  :straight nil
  :mode "\\.sh\\'"
  :commands 'org-babel-execute-source-block
  :config
  (org-babel-do-load-languages
   'org-babel-load-languages
   (append org-babel-load-languages '((shell . t))))) ;

(use-package typescript-mode
  :mode "\\.ts\\'")

(use-package password-store
  :commands password-store-get-field)

(use-package go-mode
  :mode "\\.go\\'")

(use-package telega
  :init
  (setq telega-server-libs-prefix "/usr/")
  :bind (:map telega-msg-button-map
	      ("k" . nil)
	      ("SPC" . nil))
  :config
  (setq telega-sticker-size '(8 . 24)
	telega-completing-read-function 'completing-read
	telega-chat-input-markups '(nil "org" "markdown1" "markdown2")
	telega-emoji-use-images nil)
  (set-face-attribute 'telega-entity-type-code nil :inherit 'fixed-pitch)
  (set-face-attribute 'telega-entity-type-pre nil :inherit 'fixed-pitch)
  :hook ((telega-root-mode . emojify-mode)
	 (telega-chat-mode . visual-fill-column-mode)
	 (telega-chat-mode . emojify-mode)
	 (telega-load . telega-notifications-mode))
  :commands telega)

(use-package mu4e
  :straight nil
  :load-path "site-lisp/mu4e/"
  :hook (mu4e-main-mode . visual-fill-column-mode)
  :config
  (setq mu4e-get-mail-command "mbsync -a"
	mu4e-update-interval 1800
	mu4e-sent-folder   "/sent"
	mu4e-drafts-folder "/drafts"
	mu4e-trash-folder  "/trash"
	mu4e-refile-folder "/archive"
	mu4e-attachment-dir "~/Downloads/"
	mu4e-change-filenames-when-moving t
	mu4e-confirm-quit nil)
  :commands mu4e)

(use-package mu4e-marker-icons
  :hook (mu4e-headers-found mu4e-marker-icons-mode))

(use-package mu4e-folding
  :straight (mu4e-folding :host github :repo "rougier/mu4e-folding")
  :hook (mu4e-headers-found . mu4e-folding-mode))

(use-package message
  :straight nil
  :commands message-mode
  :config
  (setq message-send-mail-function 'smtpmail-send-it
	message-kill-buffer-on-exit t))

(use-package smtpmail
  :straight nil
  :commands smtpmail-send-it
  :config
  (setq smtpmail-smtp-server "disroot.org"
	smtpmail-smtp-user "hochata"
	smtpmail-smtp-service 465
	smtpmail-stream-type 'ssl
	smtpmail-debug-info t
	smtpmail-debug-verb t
	smtpmail-servers-requiring-authorization nil
	smtpmail-local-domain "disroot.org"))

(use-package auth-source
  :straight nil
  :demand t
  :config
  (setq auth-sources '(password-store)
	auth-source-debug 'trivial
	auth-source-do-cache t
	auth-source-pass-filename "~/.password-store/auth-sources"))

(use-package mastodon
  :custom
  (mastodon-instance-url "https://emacs.ch")
  (mastodon-active-user "hochata")
  (mastodon-toot--enable-custom-instance-emoji t)
  (mastodon-tl--show-avatars t)
  (mastodon-tl--enable-proportional-fonts t)
  (mastodon-media--enable-image-caching t)
  :hook (mastodon-mode . visual-fill-column-mode)
  :commands mastodon)

(use-package empv
  :straight '(empv :type git :host github :repo "isamert/empv.el")
  :commands (empv-play empv-youtube-tabulated)
  :custom
  (empv-invidious-instance "https://y.com.sb/api/v1")
  :config
  (setq empv-mpv-args (delete "--no-video" empv-mpv-args))
  (add-to-list 'empv-mpv-args "--ytdl-format=best"))

(use-package orderless
  :custom
  (completion-styles '(orderless basic))
  (completion-category-overrides '((file (styles basic partial-completion)))))

(use-package origami
  :config
  :bind (:map origami-mode-map
	      ("C-c o" . origami-toggle-node))
  :hook (prog-mode . origami-mode))

(defun meow-setup ()
  ;;(setq meow-cheatsheet-layout meow-cheatsheet-layout-qwerty)
  (meow-motion-overwrite-define-key
   '("j" . meow-next)
   '("k" . meow-prev)
   '("<escape>" . ignore))
  (meow-leader-define-key
   ;; SPC j/k will run the original command in MOTION state.
   '("C-j" . "H-j")
   '("C-k" . "H-k")
   ;; Use SPC (0-9) for digit arguments.
   '("1" . meow-digit-argument)
   '("2" . meow-digit-argument)
   '("3" . meow-digit-argument)
   '("4" . meow-digit-argument)
   '("5" . meow-digit-argument)
   '("6" . meow-digit-argument)
   '("7" . meow-digit-argument)
   '("8" . meow-digit-argument)
   '("9" . meow-digit-argument)
   '("0" . meow-digit-argument)
   '("u" . meow-universal-argument)
   '("/" . meow-keypad-describe-key)
   '("?" . meow-cheatsheet))
  (meow-normal-define-key
   '("0" . meow-expand-0)
   '("9" . meow-expand-9)
   '("8" . meow-expand-8)
   '("7" . meow-expand-7)
   '("6" . meow-expand-6)
   '("5" . meow-expand-5)
   '("4" . meow-expand-4)
   '("3" . meow-expand-3)
   '("2" . meow-expand-2)
   '("1" . meow-expand-1)
   '("-" . negative-argument)
   '(";" . meow-reverse)
   '("," . meow-inner-of-thing)
   '("." . meow-bounds-of-thing)
   '("[" . meow-beginning-of-thing)
   '("]" . meow-end-of-thing)
   '("a" . meow-append)
   '("A" . meow-open-below)
   '("b" . meow-back-word)
   '("B" . meow-back-symbol)
   '("c" . meow-change)
   '("d" . meow-delete)
   '("D" . meow-backward-delete)
   '("e" . meow-next-word)
   '("E" . meow-next-symbol)
   '("f" . meow-find)
   '("g" . meow-cancel-selection)
   '("G" . meow-grab)
   '("h" . meow-left)
   '("H" . meow-left-expand)
   '("i" . meow-insert)
   '("I" . meow-open-above)
   '("j" . meow-next)
   '("J" . meow-next-expand)
   '("k" . meow-prev)
   '("K" . meow-prev-expand)
   '("l" . meow-right)
   '("L" . meow-right-expand)
   '("m" . meow-join)
   '("n" . meow-search)
   '("o" . meow-block)
   '("O" . meow-to-block)
   '("p" . meow-yank)
   '("q" . meow-quit)
   '("Q" . meow-goto-line)
   '("r" . meow-replace)
   '("R" . meow-swap-grab)
   '("s" . meow-kill)
   '("t" . meow-till)
   '("u" . meow-undo)
   '("U" . meow-undo-in-selection)
   '("v" . meow-visit)
   '("w" . meow-mark-word)
   '("W" . meow-mark-symbol)
   '("x" . meow-line)
   '("X" . meow-goto-line)
   '("y" . meow-save)
   '("Y" . meow-sync-grab)
   '("z" . meow-pop-selection)
   '("'" . repeat)
   '("<escape>" . ignore)))

(use-package meow
  :demand t
  :custom
  (meow-use-clipboard t)
  :config
  (setq meow-keypad-leader-dispatch ctl-x-map)
  (global-set-key (kbd "C-x u") 'meow-universal-argument)
  (meow-setup)
  (meow-global-mode 1))

(use-package vertico
  :init
  (vertico-mode))

(use-package marginalia
  :init
  (marginalia-mode))

(use-package elixir-mode
  :mode (("\\.ex\\'" . elixir-mode) ("\\.exs\\'" . elixir-mode)))

(use-package org-journal
  :bind (("C-x C-j" . org-journal-new-entry))
  :config
  (setq org-journal-dir "~/Documents/bunsan/logs/"
	org-journal-file-type 'monthly
	org-journal-enable-agenda-integration t))

;; get productive V I S U A L L Y
(use-package calfw
  :after meow
  :bind (:map override-global-map
	      ("C-x C-a" . cfw:open-org-calendar)
	      :map cfw:calendar-mode-map
	      ("t" . cfw:show-details-command))
  :config
  (add-to-list 'meow-mode-state-list '(cfw:calendar-mode . motion)))

;; get V I S U A L Org
(use-package calfw-org
  :after calfw)

(use-package org-gcal
  :commands org-gcal-sync
  :custom
  (org-gcal-client-id (password-store-get-field 'gmail.com "ClientId"))
  (org-gcal-client-secret
   (password-store-get-field 'gmail.com "ClientSecret"))
  (org-gcal-fetch-file-alist
   '(("edgar.quiroz@bunsan.io" . "~/Documents/bunsan/gcal.org"))))

(use-package rainbow-mode
  :commands rainbow-mode)

(use-package forge
  :after magit
  :config
  (add-to-list 'forge-alist
	       '("gitlab.bunsan.io"
		 "gitlab.bunsan.io/api/v4"
		 "gitlab.bunsan.io"
		 forge-gitlab-repository)))

(use-package ement
  :straight (ement :type git :host github :repo "alphapapa/ement.el")
  :init
  (add-to-list 'meow-mode-state-list '(ement-room-mode . motion))
  :custom
  (ement-save-sessions t)
  (ement-room-shr-use-fonts t)
  (ement-notify-sound "message-new-instant")
  (ement-notify-notification-predicates
   '(ement-notify--event-mentions-session-user-p
     ement-notify--event-mentions-room-p
     ement-notify--room-buffer-live-p))
  :bind (:map ement-room-mode-map
	      ("N" . ement-room-scroll-up-mark-read)
	      ("P" . ement-room-goto-fully-read-marker)
	      ("r" . ement-room-write-reply)
	      ("!" . ement-room-send-reaction)
	      ("i" . ement-room-view-event))
  :hook ((ement-room-mode . visual-fill-column-mode)
	 (ement-room-mode . emojify-mode)
	 (ement-room-mode . mixed-pitch-mode)))

(use-package dirvish
  :bind (("C-x d" . dirvish)
	 :map dirvish-mode-map
	 ("a"   . dirvish-quick-access)
	 ("N"   . dirvish-narrow)
	 ("y"   . dirvish-yank-menu)
	 ("s"   . dirvish-quicksort)
	 ("h"   . dirvish-history-jump)
	 ("TAB" . dirvish-subtree-toggle)
	 ("M-m" . dirvish-mark-menu)
	 ("M-l" . dirvish-ls-switches-menu)
	 ("M-s" . dirvish-setup-menu))
  :custom
  (dirvish-mode-line-format ; it's ok to place string inside
   '(:left (sort file-time " " file-size symlink) :right (omit yank index)))
  (dirvish-quick-access-entries
   '(("h" "~/" "Home")
     ("d" "~/Downloads/" "Downloads")
     ("r" "~/Documents/repos" "Repos")
     ("w" "~/Documents/bunsan" "Work")
     ("p" "~/Documents/bs" "Personal")))
  (dirvish-attributes '(all-the-icons file-size subtree-state vc-state))
  :init
  (dirvish-override-dired-mode))

(use-package ledger-mode
  :mode "\\.journal\\'"
  :hook (ledger-mode . visual-fill-column-mode)
  :config
  (setq
   ledger-binary-path "hledger"
   ledger-mode-should-check-version nil
   ledger-report-links-in-register nil
   ledger-report-auto-width nil
   ledger-report-use-native-highlighting nil))

(use-package mix
  :config
  (define-key mix-minor-mode-map (kbd "C-c") 'mix-minor-mode-command-map)
  :hook
  (elixir-mode . mix-minor-mode))

(use-package nov
  :custom
  (nov-text-width t)
  (nov-variable-pitch nil)
  :mode ("\\.epub\\'" . nov-mode)
  :hook ((nov-mode . visual-fill-column-mode)
	 (nov-mode . visual-line-mode)))

(use-package punpun-themes
  :commands load-theme)

(use-package nov-xwidget
  :straight (nov-xwidget :type git :host github :repo "chenyanming/nov-xwidget")
  :after nov
  :bind (:map nov-mode-map
	      ("o" . 'nov-xwidget-view))
  :hook (nov-mode . nov-xwidget-inject-all-files))

(use-package calibredb
  :commands calibredb
  :config
  (setq calibredb-root-dir "~/Documents/calibre")
  (setq calibre-format-all-the-icons t)
  (setq calibredb-db-dir (expand-file-name "metadata.db" calibredb-root-dir))
  (setq calibredb-library-alist '(("~/Documents/books")))
  (add-to-list 'meow-mode-state-list '(calibredb-search-mode . motion)))

(use-package ox-reveal
  :commands (org-reveal-export-to-html-and-browse
	     org reveal-export-to-html
	     org-reveal-export-current-subtree))

(use-package denote
  :custom
  (denote-directory "~/Documents/bs/notes/")
  (denote-known-keywords '("emacs" "elixir" "web" "math" "cs" "bunsan" "protocols"))
  (denote-dired-directories '("~/Documents/bs/notes/" "~/Documents/bunsan/notes/"))
  :hook (dired-mode . denote-dired-mode))

(defun eqf0/org-present-start ()
  (setq-local face-remapping-alist
	      '((default (:height 1.5) variable-pitch)
		(header-line (:height 4.0) variable-pitch)
		(org-document-title (:height 1.75) org-document-title)
		(org-block (:height 1.25) org-block)
		(org-block-begin-line (:height 0.7) org-block)))
  (org-display-inline-images)
  (org-present-hide-cursor)
  (org-present-read-only)
  (setq header-line-format " "))

(defun eqf0/org-present-end ()
  (setq header-line-format nil)
  (setq-local face-remapping-alist '((default fixed-pitch default)))
  (org-remove-inline-images)
  (org-present-show-cursor)
  (org-present-read-write))

(defun eqf0/org-present-prepare-slide (buffer-name heading)
  (org-overview)
  (org-show-entry)
  (org-show-children))

(use-package org-present
  :config
  (add-hook 'org-present-after-navigate-functions 'eqf0/org-present-prepare-slide)
  :hook ((org-present-mode . eqf0/org-present-start)
	 (org-present-mode-quit . eqf0/org-present-end)))

(use-package eimp
  :commands eimp-mode)

(defun eqf0/erc-connect (server)
  (erc-tls
   :server server
   :port 6697
   :nick "hochata"
   :full-name "hochata"))

(defun eqf0/erc-connect-pine ()
  (interactive)
  (eqf0/erc-connect "irc.pine64.org"))

(defun eqf0/erc-connect-libera ()
  (interactive)
  (eqf0/erc-connect "irc.libera.chat"))

(defun eqf0/erc-connect-libera ()
  (interactive)
  (eqf0/erc-connect "irc.libera.chat"))

(defun eqf0/erc-connect-oftc ()
  (interactive)
  (eqf0/erc-connect "irc.oftc.net"))

(use-package erc
  :commands (eqf0/erc-connect-libera eqf0/erc-connect-pine eqf0/erc-connect-oftc)
  :straight nil
  :hook (erc-mode . visual-fill-column-mode)
  :custom
  (erc-prompt-for-password nil)
  (erc-interpret-mirc-color t)
  (erc-autojoin-channels-alist
   '(("libera.chat" "#emacs-es" "#haskell" "#emacs" "#systemcrafters" "#elixir" "#nim" "#nim-offtopic")
     ("pine64.org" "#pinephone" "#pinebook" "#Pine64"))))

(use-package eldoc
  :straight nil
  :commands eldoc-display-in-buffer
  :custom
  (eldoc-display-functions '(eldoc-display-in-buffer)))

(defun +eglot-flymake ()
  (flymake-mode t)
  (add-hook 'flymake-diagnostic-functions 'eglot-flymake-backend nil t))

(use-package eglot
  :commands eglot
  :init
  (setq eglot-stay-out-of '(flymake))
  :hook
  (eglot-managed-mode . +eglot-flymake)
  :config
  (add-to-list 'eglot-ignored-server-capabilities :hoverProvider))

(use-package eglot-elixir
  :commands eglot
  :straight (:type git :host github :repo "bvnierop/eglot-elixir"))

(use-package kind-icon
  :ensure t
  :after corfu
  :custom
  (kind-icon-blend-background nil)
  :config
  (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter))

(use-package ef-themes
  :hook (ef-themes-post-load . +subtle-whitespace)
  :custom
  (ef-themes-to-toggle '(ef-cyprus ef-bio))
  :init
  (ef-themes-select 'ef-cyprus))

(use-package warnings
  :straight nil
  :custom
  (warning-suppress-types '((comp))))

(use-package mini-frame
  :init
  (mini-frame-mode 1)
  :custom
  (mini-frame-detach-on-hide nil)
  (mini-frame-resize t)
  (mini-frame-show-parameters
   '((left . 0)
     (top . 0)
     (width . 1.0)
     (height . 1))))

;; I really need to finish my thesis...
;; https://github.com/radian-software/straight.el/issues/836
(use-package auctex
  :straight (:type git :host nil :repo "https://git.savannah.gnu.org/git/auctex.git"
		   :pre-build (("./autogen.sh")
			       ("./configure" "--without-texmf-dir" "--with-lispdir=.")
			       ("make")))
  :mode ("\\.tex\\'" . latex-mode)
  :hook
  (LaTeX-mode . TeX-PDF-mode)
  (LaTeX-mode . flyspell-mode)
  (LaTeX-mode . flycheck-mode)
  (LaTeX-mode . LaTeX-math-mode)
  :init
  (load "auctex.el" nil t t)
  (load "preview-latex.el" nil t t)
  (setq-default TeX-master nil)
  (setq TeX-data-directory (straight--repos-dir "auctex")
	TeX-lisp-directory TeX-data-directory
	preview-TeX-style-dir (concat ".:" (straight--repos-dir "auctex") "latex:")
	TeX-parse-self t
	TeX-auto-save t
	TeX-view-program-selection '((output-pdf "PDF Tools"))))

(use-package mermaid-mode
  :mode "\\.mmd\\'")

(use-package ob-mermaid
  :after org
  :config
  (org-babel-do-load-languages
   'org-babel-load-languages
   (append org-babel-load-languages '((mermaid . t)))))

(use-package htmlize
  :commands htmlize-buffer)

(use-package hare-mode
  :straight (:type git :repo "https://git.sr.ht/~bbuccianti/hare-mode")
  :mode "\\.ha\\'")

(use-package feebleline
  :commands feebleline-mode)

(use-package eglot-jl
  :commands eglot)

(use-package csharp-mode
  :mode "\\.cs\\'")

(use-package mentor
  :commands mentor)

(use-package osm
  :bind ("C-x w" . osm-home)
  :custom
  (osm-server 'stamen-toner-dark))

(use-package elfeed
  :commands elfeed
  :custom
  (elfeed-feeds
   '(("https://sachachua.com/blog/feed/" emacs)
     ("https://protesilaos.com/books.xml" life)
     ("https://drewdevault.com/blog/index.xml" code linux)
     ("https://protesilaos.com/codelog.xml" emacs code)
     ("https://api.quantamagazine.org/feed/" science math)
     ("https://postmarketos.org/blog/feed.atom" linux mobile)
     ("https://linmob.net/feed.xml" linux)
     ("https://www.youtube.com/feeds/videos.xml?channel_id=UC7YOGHUfC1Tb6E4pudI9STA" linux)
     ("https://www.youtube.com/feeds/videos.xml?channel_id=UCld68syR8Wi-GY_n4CaoJGA" linux)
     ("https://www.youtube.com/feeds/videos.xml?channel_id=UCWf43GShTqMDdJN9pICYd2Q" life)
     ("https://www.youtube.com/feeds/videos.xml?channel_id=UC7-E5xhZBZdW-8d7V80mzfg" life)
     ("https://www.youtube.com/feeds/videos.xml?channel_id=UCTpmmkp1E4nmZqWPS-dl5bg" science)
     ("https://www.youtube.com/feeds/videos.xml?channel_id=UCrq3JYirgV-BLluzTF6X_7A" life)
     ("https://www.youtube.com/feeds/videos.xml?channel_id=UCYO_jab_esuFRV4b17AJtAw" science math)
     ("https://www.youtube.com/feeds/videos.xml?channel_id=UCoxcjq-8xIDTYp3uz647V5A" math)
     ("https://www.youtube.com/feeds/videos.xml?channel_id=UCzBjutX2PmitNF4avysL-vg" math)
     ("https://www.youtube.com/feeds/videos.xml?channel_id=UCtGG8ucQgEJPeUPhJZ4M4jA" life)
     ("https://www.youtube.com/feeds/videos.xml?channel_id=UCOGeU-1Fig3rrDjhm9Zs_wg" math))))

(use-package elfeed-tube
  :commands (elfeed-tube-setup)
  :init
  (add-hook 'elfeed-update-init-hooks 'elfeed-tube-setup)
  :bind (:map elfeed-show-mode-map
	      ("F" . elfeed-tube-fetch)
	      ([remap save-buffer] . elfeed-tube-save)
	      :map elfeed-search-mode-map
	      ("F" . elfeed-tube-fetch)
	      ([remap save-buffer] . elfeed-tube-save)))

(use-package elfeed-tube-mpv
  :bind (:map elfeed-show-mode-map
	      ("C-c C-f" . elfeed-tube-mpv-follow-mode)
	      ("C-c C-w" . elfeed-tube-mpv-where)))

(use-package apheleia
  :config
  (add-to-list 'apheleia-formatters '(purescript-tidy "purs-tidy" "format"))
  (add-to-list 'apheleia-mode-alist '(purescript-mode . purescript-tidy))
  :commands apheleia-format-buffer)

(use-package jsonian
  :straight (:type git :host github :repo "iwahbe/jsonian")
  :mode "\\.json\\'"
  :after so-long
  :custom
  (jsonian-no-so-long-mode))

(use-package yequake
  :commands yequake-toggle
  :custom
  (yequake-frames
   '(("Yeq Telega" .
      ((width . 0.75)
       (height . 0.75)
       (alpha . 1.0)
       (buffer-fns . (telega))
       (frame-parameters . ((undecorated . t)))))
     ("Yeq Elfeed" .
      ((width . 0.75)
       (height . 0.75)
       (alpha . 1.0)
       (buffer-fns . (eqf0/elfeed-bring))
       (frame-parameters . ((undecorated . t)))))
     ("Yeq Config" .
      ((width . 0.75)
       (height . 0.75)
       (alpha . 1.0)
       (buffer-fns . ("~/.config/emacs/init.el"))
       (frame-parameters . ((undecorated . t)))))
     ("Yeq Ement" .
      ((width . 0.75)
       (height . 0.75)
       (alpha . 1.0)
       (buffer-fns . (eqf0/ement-bring))
       (frame-parameters . ((undecorated . t)))))
     ("Yeq Mastodon" .
      ((width . 0.75)
       (height . 0.75)
       (alpha . 1.0)
       (buffer-fns . (mastodon))
       (frame-parameters . ((undecorated . t)))))
     ("Yeq Todo" .
      ((width . 0.75)
       (height . 0.75)
       (alpha . 1.0)
       (buffer-fns . ("~/Documents/bs/todo.org"))
       (frame-parameters . ((undecorated . t)))))
     ("Yeq Vterm" .
      ((width . 0.75)
       (height . 0.75)
       (alpha . 1.0)
       (buffer-fns . (vterm))
       (frame-parameters . ((undecorated . t)))))
     ("Yeq Mu" .
      ((width . 0.75)
       (height . 0.75)
       (alpha . 1.0)
       (buffer-fns . (mu4e))
       (frame-parameters . ((undecorated . t))))))))

(defun eqf0/elfeed-bring ()
  (interactive)
  (elfeed)
  (elfeed-search-buffer))

(defun eqf0/ement-bring ()
  (interactive)
  (if ement-sessions
      (ement-room-list)
    (call-interactively 'ement-connect)))

(use-package org-mime
  :commands org-mime-htmlize)

(use-package erlang
  :mode "\\.erl\\'")

(define-derived-mode eqf0/vue-mode web-mode "eqf0/Vue"
  "A major mode derived from web-mode, for editing .vue files with LSP support.")

(add-hook 'eqf0/vue-mode-hook 'eqf0/flymake-eslint-enable)

(add-to-list 'auto-mode-alist '("\\.vue\\'" . eqf0/vue-mode))

(eval-after-load 'eglot
  '(add-to-list 'eglot-server-programs
	       '(eqf0/vue-mode . ("vue-language-server" "--stdio"
			    :initializationOptions
			    (:typescript
			     (:tsdk "/home/quire/.nvm/versions/node/v19.2.0/lib/node_modules/typescript/lib"))))))

(defun eqf0/fix-vue-faces ()
  (interactive)
  (when (bound-and-true-p eqf0/vue-mode-map)
      (set-face-attribute
       'web-mode-html-tag-face
       nil
       :foreground nil
       :inherit 'font-lock-function-name-face)
    (set-face-attribute
     'web-mode-html-attr-name-face
     nil
     :foreground nil
     :inherit 'font-lock-variable-name-face)))

(use-package web-mode
  :custom
  (web-mode-indent-style 1)
  (web-mode-css-indent-offset 2)
  (web-mode-sql-indent-offset 2)
  (web-mode-code-indent-offset 2)
  (web-mode-attr-value-indent-offset 2)
  (web-mode-markup-indent-offset 2)
  (web-mode-script-padding nil)
  (web-mode-style-padding nil)
  (web-mode-block-padding nil)
  (web-mode-part-padding nil)
  :hook ((eqf0/vue-mode . eqf0/fix-vue-faces)))
	 ;; (ef-themes-post-load . eqf0/fix-vue-faces)))

(use-package js
  :straight nil
  :mode ("\\.js\\'" . js-mode)
  :hook (js-mode . eqf0/flymake-eslint-enable)
  :custom
  (js-indent-level 2))

(defun eqf0/flymake-eslint-enable ()
  (interactive)
  (when-let* ((root (project-root (project-current t)))
	      (rc (locate-file "package" (list root) '(".json"))))
    (setq-local flymake-eslint-project-root root)
    (make-local-variable 'exec-path)
    (push (file-name-concat root "node_modules" ".bin") exec-path))
  (flymake-eslint-enable))

(use-package flymake-eslint
  :commands flymake-eslint-enable)

(use-package css-mode
  :straight nil
  :mode "\\.css\\'"
  :custom
  (css-indent-offset 2))

(use-package screenshot
  :straight (:type git :host github :repo "tecosaur/screenshot")
  :commands screenshot)

(use-package standard-themes)

(use-package purescript-mode
  :custom (purescript-indentation-cycle-warn t)
  :hook (purescript-mode . purescript-indentation-mode))

(use-package dhall-mode
  :custom
  (dhall-use-header-line nil)
  (dhall-format-at-save nil))

(use-package dockerfile-mode
  :mode "\\.css\\'"
  :custom
  (css-indent-offset 2))

(use-package screenshot
  :straight (:type git :host github :repo "tecosaur/screenshot")
  :commands screenshot)

(use-package standard-themes)

(use-package purescript-mode
  :custom (purescript-indentation-cycle-warn t)
  :hook (purescript-mode . purescript-indentation-mode))

(use-package dhall-mode
  :custom
  (dhall-use-header-line nil)
  (dhall-format-at-save nil))

(use-package dockerfile-mode
  :mode "Dockerfile")

(use-package docker-compose-mode
  :mode "\\(docker-\\)?compose\\.ya?ml\\'")

(use-package eat
  :straight  (:type git
	      :repo "https://codeberg.org/akib/emacs-eat"
	      :files ("*.el" ("term" "term/*.el") "*.texi"
		      "*.ti" ("terminfo/e" "terminfo/e/*")
		      ("terminfo/65" "terminfo/65/*")
		      ("integration" "integration/*")
		      (:exclude ".dir-locals.el" "*-tests.el"))))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-agenda-files
   '("/home/quire/Documents/bunsan/gcal.org" "/home/quire/Documents/bs/todo.org" "/home/quire/Documents/bunsan/logs/20221201") nil nil "Customized with use-package org-agenda"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
