;;; +flutter.el --- flutter and dart configuration   -*- lexical-binding: t; -*-

;; Copyright (C) 2023  hochata

;; Author: hochata <hochata@disroot.org>
;; Keywords: languages

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(with-eval-after-load 'treesit
  (add-to-list 'treesit-language-source-alist
	       '(dart "https://github.com/UserNobody14/tree-sitter-dart")))

(unless (package-installed-p 'dart-mode)
  (package-install 'dart-mode))

(unless (package-installed-p 'flutter)
  (package-install 'flutter))

(add-hook 'dart-mode-hook 'flutter-test-mode)
(add-hook 'dart-mode-hook 'apheleia-mode-maybe)
(add-hook 'dart-mode-hook 'eglot-ensure)

(defun +flutter-run-or-reload ()
  (interactive)
  (if (flutter--running-p)
      (flutter-hot-reload)
    (call-interactively #'flutter-run)))

(with-eval-after-load 'dart-mode
  (define-key dart-mode-map (kbd "C-c e") #'+flutter-run-or-reload))

(with-eval-after-load 'project
  (add-to-list 'project-vc-extra-root-markers "pubspec.yaml"))

(with-eval-after-load 'apheleia
  (setf (alist-get 'dart-format apheleia-formatters)
	'("dart" "format" "-l" "120")))

(provide '+flutter)
;;; +flutter.el ends here
