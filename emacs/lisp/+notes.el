;;; +notes.el --- notes and book                 -*- lexical-binding: t; -*-

;;; Commentary:

;; Configuration for notes and library managment.

;;; Code:

(unless (package-installed-p 'ox-pandoc)
  (package-install 'ox-pandoc))

(setq doc-view-resolution 144
      doc-view-continuous t
      doc-view-mupdf-use-svg t)

(unless (package-installed-p 'calibredb)
  (package-install 'calibredb))

(with-eval-after-load 'meow
  (add-to-list
   'meow-mode-state-list
   '(calibredb-search-mode . motion)))

(unless (package-installed-p 'nov)
  (package-install 'nov))

(add-hook 'nov-mode-hook 'visual-fill-column-mode)

(add-hook 'Info-mode-hook 'visual-fill-column-mode)

(unless (package-installed-p 'denote)
  (package-install 'denote))

(setq denote-org-capture-specifiers "* %?\n%T\n%i\n")

(defun +scoped-note (work-dir sub-dir extra-tags)
  (when (require 'denote nil 'noerror)
    (let ((denote-directory work-dir)
	  (full-dir (expand-file-name sub-dir work-dir)))
      (denote
       (denote-title-prompt)
       (append extra-tags (denote-keywords-prompt))
       nil
       full-dir))))

(defgroup +notes nil "Utilities for note taking")

(defcustom +notes-work-root "~/Documents/notes/"
  "Base directory to save work notes"
  :type 'string :safe 'directory-name-p :group '+notes)

(defun +meeting-note ()
  (interactive)
  (+scoped-note +notes-work-root "meetings" '("meeting")))

(defun +work-research-note ()
  (interactive)
  (+scoped-note +notes-work-root "notes" '("notes")))

(defun +course-note ()
  (interactive)
  (+scoped-note +notes-work-root "courses" '("courses")))

(setq org-refile-targets
      '((nil :maxlevel . 9)
	(org-agenda-files :maxlevel . 9)))

(unless (package-installed-p 'tmr)
  (package-install 'tmr))

(unless (package-installed-p 'ledger-mode)
  (package-install 'ledger-mode))

(setq ledger-binary-path "hledger"
      ledger-mode-should-check-version nil)

(unless (package-installed-p 'typst-ts-mode)
  (package-vc-install "https://git.sr.ht/~meow_king/typst-ts-mode"))

(with-eval-after-load 'treesit
  (add-to-list 'treesit-language-source-alist
	       '(typst . ("https://github.com/uben0/tree-sitter-typst"))))

(provide '+notes)
;;; +notes.el ends here
