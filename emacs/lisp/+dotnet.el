;;; +dotnet.el --- c# and related ide configuration  -*- lexical-binding: t; -*-

;;; Commentary:

;;

;;; Code:

(with-eval-after-load 'treesit
  (add-to-list 'treesit-language-source-alist
	       '(c-sharp "https://github.com/tree-sitter/tree-sitter-c-sharp")))
(add-to-list 'major-mode-remap-alist '(csharp-mode . csharp-ts-mode))

(unless (package-installed-p 'sharper)
  (package-install 'sharper))

(autoload 'sharper-main-transient "sharper" ".NET transient menu" t)

(with-eval-after-load 'csharp-mode
  (define-key csharp-mode-map (kbd "C-c e") 'sharper-main-transient)
  (define-key csharp-mode-map (kbd "TAB") 'completion-at-point)
  (define-key csharp-ts-mode-map (kbd "C-c e") 'sharper-main-transient)
  (define-key csharp-ts-mode-map (kbd "TAB") 'completion-at-point))

(with-eval-after-load 'apheleia-formatters
  (add-to-list
   'apheleia-formatters
   '(csharpier "dotnet" "csharpier"))
  (add-to-list
   'apheleia-mode-alist '(csharp-mode . csharpier))
  (add-to-list
   'apheleia-mode-alist '(csharp-ts-mode . csharpier)))

(define-derived-mode csproj-mode xml-mode "CSProj"
  "A major mode for editing csproj and other msbuild-style project files"
  :group 'csproj
  (define-key csproj-mode-map (kbd "C-c e") 'sharper-main-transient))

(add-to-list 'auto-mode-alist '("\\.[^.]*proj\\'" . csproj-mode))

(with-eval-after-load 'project
  (add-to-list
   'project-vc-extra-root-markers
   "*.csproj"))

(provide '+dotnet)
;;; +dotnet.el ends here
