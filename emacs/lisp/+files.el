;;; +files.el --- file management                -*- lexical-binding: t; -*-

;;; Commentary:

;; Bunch of extra configuration to make file managment nicer.

;;; Code:

(setq backup-directory-alist
      `(("." . ,(locate-user-emacs-file "tmp/backups/")))
      backup-by-copying t
      delete-old-versions t
      version-control t
      auto-save-file-name-transforms
      `((".*" ,(locate-user-emacs-file "tmp/autosaves") t)))

(setq async-shell-command-display-buffer nil)

(setq vc-follow-symlinks t)

(setq custom-file (locate-user-emacs-file "custom.el"))
(when (file-exists-p custom-file)
  (load custom-file))

(unless (package-installed-p 'nerd-icons-dired)
  (package-install 'nerd-icons-dired))

(unless (package-installed-p 'dired-subtree)
  (package-install 'dired-subtree))

(with-eval-after-load 'dired
  (define-key global-map (kbd "C-x d") 'dired-jump)
  (define-key dired-mode-map (kbd "<tab>") 'dired-subtree-toggle)
  (when (equal system-type 'darwin)
    (define-key dired-mode-map (kbd "«") 'dired-up-directory))
  (setq dired-compress-directory-default-suffix ".zip"
	dired-compress-file-default-suffix ".zip"
	dired-kill-when-opening-new-dired-buffer t
	dired-listing-switches "-Al"))

(add-hook 'dired-mode-hook 'dired-hide-details-mode)
(add-hook 'dired-mode-hook #'nerd-icons-dired-mode)

(global-auto-revert-mode)

(setq proced-auto-update-interval 1)
(customize-set-variable 'proced-auto-update-flag t)


(provide '+files)
;;; +files.el ends here
