;;; +buffers.el --- buffer managment                 -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

(unless (package-installed-p 'yequake)
  (package-install 'yequake))

(defun +yeq-add (title callback)
  (when (require 'yequake nil 'noerror)
    (let ((full-title (concat "Yeq " title)))
      (add-to-list
       'yequake-frames
       `( ,full-title .
	  ((width . 0.75)
	   (height . 0.75)
	   (alpha . 1.0)
	   (buffer-fns . (,callback))
	   (frame-parameters . ((undecorated . t)))))))))

(defun +other-prev-window ()
    (interactive)
    (other-window -1 nil nil))

(define-key global-map (kbd "C-x O") #'+other-prev-window)

(unless (package-installed-p 'shackle)
  (package-install 'shackle))

(setopt magit-display-buffer-function 'magit-display-buffer-fullframe-status-v1)

(setq shackle-rules
      '((compilation-mode :noselect t :align below :size 0.25)
	(jest-test-compilation-mode :noselect t :align below :size 0.25)
	(messages-buffer-mode :noselect t :align below :size 0.25)
	(comint-mode :noselect t :align below :size 0.25)
	("\\`magit-revision:.*\\'" :regexp t :align right :size 0.7)
	("*eldoc*" :select t :align below :size 0.25)
	(shell-mode :select t :align below :size 0.25)
	("Backtrace" :noselect t :align below :size 0.25)
	("Warnings" :regexp t :noselect t :align below :size 0.25)
	("ellama" :regexp t :select t :align below :size 0.25)
	("Embark Actions" :regexp t :align below :size 0.25)
	(completion-list-mode :align below :size 0.25)
	(helpful-mode :select t :align below :size 0.25)
	(help-mode :select t :align below :size 0.25)
	(flymake-project-diagnostics-mode :select t :align below :size 0.25)
	(xref--xref-buffer-mode :select t :align below :size 0.3)
	(flutter-mode :select nil :align below :size 0.3)
	(sql-interactive-mode :select t :align below :size 0.3)))

(when (require 'shackle nil 'noerror)
  (shackle-mode 1))

(provide '+buffers)
;;; +buffers.el ends here
