;;; +completions.el --- completion framework configuration  -*- lexical-binding: t; -*-

;;; Commentary:

;; Completions in Icomplete-mode, *Completions*, minubuffer.

;;; Code:
(setq completions-format 'one-column
      completions-header-format nil
      completions-max-height 10
      completions-sort nil
      completions-detailed t)

(setq completion-cycling nil
      completion-ignore-case t
      completion-show-help nil)

(define-key
 completion-in-region-mode-map (kbd "<down>") 'minibuffer-next-completion)
(define-key
 completion-in-region-mode-map (kbd "<up>") 'minibuffer-previous-completion)
(define-key
 completion-in-region-mode-map (kbd "RET") 'minibuffer-choose-completion)

(define-key minibuffer-mode-map (kbd "<down>") 'minibuffer-next-completion)
(define-key minibuffer-mode-map (kbd "<up>") 'minibuffer-previous-completion)

(unless (package-installed-p 'corfu)
  (package-install 'corfu))

(setq corfu-auto t)
(global-corfu-mode)

(unless (package-installed-p 'nerd-icons-corfu)
  (package-install 'nerd-icons-corfu))

(with-eval-after-load 'corfu
  (add-to-list 'corfu-margin-formatters #'nerd-icons-corfu-formatter))

(setq icomplete-scroll t
      icomplete-show-matches-on-no-input t
      icomplete-in-buffer nil)

(with-eval-after-load 'icomplete
  (define-key
    icomplete-vertical-mode-minibuffer-map (kbd "TAB") 'icomplete-force-complete)
  (define-key
    icomplete-vertical-mode-minibuffer-map
    (kbd "RET") 'icomplete-force-complete-and-exit)
  (define-key
    icomplete-vertical-mode-minibuffer-map (kbd "M-RET") 'icomplete-fido-exit)
  (define-key
    icomplete-vertical-mode-minibuffer-map
    (kbd "C-d") 'icomplete-fido-delete-char))

(icomplete-vertical-mode)

(setq use-short-answers t)

(setq tab-always-indent 'complete)

(unless (package-installed-p 'orderless)
  (package-install 'orderless))

(when (require 'orderless nil 'noerror)
  (setq completion-styles '(orderless basic)
	completion-category-overrides
	'((file (styles basic partial-completion)))))

(unless (package-installed-p 'nerd-icons-completion)
  (package-install 'nerd-icons-completion))

(add-hook 'marginalia-mode-hook #'nerd-icons-completion-marginalia-setup)

(unless (package-installed-p 'marginalia)
  (package-install 'marginalia))

(setq marginalia-field-width 60)
(marginalia-mode 1)

(unless (package-installed-p 'embark)
  (package-install 'embark))

(define-key global-map (kbd "C-.") #'embark-act)
(define-key global-map (kbd "M-.") #'embark-dwim)
(define-key global-map (kbd "C-h B") #'embark-bindings)

(setq prefix-help-command #'embark-prefix-help-command)

(provide '+completions)
;;; +completions.el ends here
