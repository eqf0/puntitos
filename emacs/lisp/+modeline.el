;;; +modeline.el --- mode line configuration         -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

(require 'nerd-icons)

(defun +ml-current-mode-icon ()
  (let ((icon (nerd-icons-icon-for-mode major-mode :height 1.5 :v-adjust -0.1)))
    (if (stringp icon)
	(propertize icon
		    'help-echo (format "Major mode: `%s`" major-mode))
      "")))

(defun +ml-meow-indicator ()
  (if (and (require 'meow nil 'noerror) meow-mode)
    (propertize
     (meow-indicator)
     'help-echo (format "Meow state: %s" (meow-indicator))
     'face '(font-lock-builtin-face (:inherit fixed-pitch))))
   "")

(defun +ml--buffer-face ()
  (cond
   (buffer-read-only
    '(font-lock-constant-face (:inherit fixed-pitch)))
   ((buffer-modified-p)
    '(font-lock-warning-face (:inherit fixed-pitch :weight bold)))
   (t
    '(font-lock-string-face (:inherit fixed-pitch)))))

(defun +ml-buffer-name ()
  (let ((buff-name (buffer-name)))
    (propertize
     (if (length> buff-name 57)
	 (concat (truncate-string-to-width buff-name 50) "...")
       buff-name)
     'face (+ml--buffer-face)
     'help-echo (buffer-file-name))))

(defun +ml-region-count ()
  (when mark-active
    (let ((lines (count-lines (region-beginning) (region-end)))
	  (words (count-words (region-end) (region-beginning))))
      (propertize
       (format "(%s,%s)" lines words)
       'face '(font-lock-function-name-face)))))

(defun +ml-cursor-abs-pos ()
  (propertize
   "(%l:%c)"
   'face '(font-lock-constant-face)))

(defun +ml-cursor-rel-pos ()
   (propertize
    "%p "
    'face '(font-lock-constant-face)))

(defun +ml-minor-modes ()
  (if (require 'minions nil 'noerror)
      minions-mode-line-modes
    mode-lines-modes))

(defun +ml-git-info ()
  (when vc-mode
    (let ((branch (mapconcat 'concat (cdr (split-string vc-mode "[:-]")) "-")))
      (propertize
       (format
	" %s %s"
	(nerd-icons-sucicon "nf-seti-git")
	branch)
       'face '(vc-locked-state)))))

(defun +ml-flymake-counter ()
  (when (and (bound-and-true-p flymake-mode) (flymake-running-backends))
    '(""
      flymake-mode-line-error-counter
      flymake-mode-line-warning-counter
      flymake-mode-line-note-counter
      " ")))

(defun +ml-eglot-status ()
  (when (bound-and-true-p eglot--managed-mode)
      (list (eglot--mode-line-format) " ")))

(defvar left-mode-line
  (list
   " "
   '(:eval (+ml-current-mode-icon))
   "  "
   '(:eval (+ml-minor-modes))
   '(:eval (+ml-buffer-name))))

(defvar rigth-mode-line
  (list
   '(:eval mode-line-position)
   '(:eval (+ml-region-count))
   '(:eval (+ml-flymake-counter))
   '(:eval (+ml-eglot-status))
   '(:eval (+ml-git-info))
   " "))

(defun +ml--len (content)
  (string-width (format-mode-line content)))

(defun +ml-padding ()
  (let ((r-pad (+ml--len rigth-mode-line))
	(l-pad (+ml--len left-mode-line)))
    (propertize
     " "
     'face '(fixed-pitch)
     'display
	 `((space :align-to (- right ,r-pad 1))))))

(customize-set-variable
 'mode-line-format
 (list
  left-mode-line
  '(:eval (+ml-padding))
  rigth-mode-line))

(provide '+modeline)
;;; +modeline.el ends here
