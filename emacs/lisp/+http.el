;;; +http.el --- http utilities                      -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Edgar Quiroz

;; Author: Edgar Quiroz <edgarquiroz@192.168.1.4>
;; Keywords: c

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(unless (package-installed-p 'hurl-mode)
  (package-vc-install "https://github.com/JasZhe/hurl-mode"))

(add-to-list 'org-babel-load-languages '(hurl . t))

(add-to-list 'auto-mode-alist '("\\.hurl\\'" . hurl-mode))

(provide '+http)
;;; +http.el ends here
