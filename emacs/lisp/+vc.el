;;; +vc.el --- interaction with version control systems  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Edgar Quiroz

;; Author: Edgar Quiroz <edgarquiroz@192.168.1.3>
;; Keywords: vc

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(unless (package-installed-p 'forge)
  (package-install 'forge))

(unless (package-installed-p 'magit)
  (package-install 'magit))

(setq vc-ignore-dir-regexp
      (format "\\(%s\\)\\|\\(%s\\)"
	      vc-ignore-dir-regexp
	      tramp-file-name-regexp))

(provide '+vc)
;;; +vc.el ends here
