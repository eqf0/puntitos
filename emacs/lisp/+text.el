;;; +text.el --- plain text possibilities        -*- lexical-binding: t; -*-

;;; Commentary:

;; Configuration for different plain text formats.

;;; Code:

(eval-after-load 'org-babel
  (org-babel-do-load-languages 'org-babel-load-languages org-babel-load-languages))

(unless (package-installed-p 'markdown-mode)
  (package-install 'markdown-mode))

(add-hook 'markdown-mode-hook 'visual-fill-column-mode)

(with-eval-after-load 'markdown-mode
  (customize-set-variable 'markdown-hide-markup t)
  (customize-set-variable 'markdown-hide-urls t)
  (customize-set-variable 'markdown-enable-math t)
  (customize-set-variable 'markdown-fontify-code-blocks-natively t)
  (customize-set-variable 'markdown-header-scaling nil))

(add-hook 'markdown-mode-hook 'mixed-pitch-mode)
(add-hook 'markdown-mode-hook 'visual-line-mode)

(unless (package-installed-p 'org-modern)
  (package-install 'org-modern))

(add-hook 'org-mode-hook 'mixed-pitch-mode)
(add-hook 'org-mode-hook 'visual-line-mode)
(add-hook 'org-mode-hook #'org-modern-mode)

(setq org-src-preserve-indentation t)

(unless (package-installed-p 'mermaid-mode)
  (package-install 'mermaid-mode))

(unless (package-installed-p 'jinx)
  (package-install 'jinx))

(keymap-global-set "<remap> <ispell-word>" #'jinx-correct)

(add-hook 'text-mode-hook 'jinx-mode)
(add-hook 'org-mode-hook 'jinx-mode)
(add-hook 'markdown-mode-hook 'jinx-mode)

(unless (package-installed-p 'tempel)
  (package-install 'tempel))

(defun +tempel-setup-capf ()
  (setq-local completion-at-point-functions
	      (cons #'tempel-expand
		    completion-at-point-functions)))

(add-hook 'prog-mode-hook '+tempel-setup-capf)
(add-hook 'text-mode-hook '+tempel-setup-capf)

(unless (package-installed-p 'tempel-collection)
  (package-install 'tempel-collection))

(provide '+text)
;;; +text.el ends here
