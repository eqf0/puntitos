;;; +erlang.el --- erlang ide conf                   -*- lexical-binding: t; -*-

;;; Commentary:

;;

;;; Code:

;; how to set this automatically?
(defvar erlang-tools-ver "3.6")

(when (executable-find "asdf")

  (setq erlang-root-dir
	(string-trim
	 (shell-command-to-string "asdf where erlang")))

  (add-to-list 'load-path
  	       (concat
  		erlang-root-dir
		"/lib/tools-"
		erlang-tools-ver
		"/emacs"))

  (require 'erlang-start))

(provide '+erlang)
;;; +erlang.el ends here
