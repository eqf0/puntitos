;;; +ide.el --- code editing facilities         -*- lexical-binding: t; -*-

;;; Commentary:

;; Common configuration for the code utilities: eldoc, flymake, eglot,
;; as well as some third party packages. Language specific support exists in
;; separate files.

;;; Code:

(setq grep-use-null-filename-separator t)

(with-eval-after-load 'treesit
  (setopt treesit-font-lock-level 4))

(add-hook 'prog-mode-hook 'repeat-mode)
(add-hook 'prog-mode-hook 'electric-pair-mode)
(add-hook 'prog-mode-hook 'display-line-numbers-mode)
(add-hook 'prog-mode-hook 'whitespace-mode)

(define-key prog-mode-map (kbd "C-c n") 'flymake-goto-next-error)
(define-key prog-mode-map (kbd "C-c p") 'flymake-goto-prev-error)

(defun +eglot-eldoc-setup-keys ()
  (define-key special-mode-map (kbd "RET") 'markdown-do))

(defun +eglot-eldoc-disable-md-scaling ()
  (with-current-buffer (get-buffer " *Echo Area 0*")
    (setq-local
     face-remapping-alist
     `((markdown-header-face-3
	(:height ,(/ 1 1.2) :weight normal) ef-themes-heading-3)))))

(defun +eglot-eldoc ()
  (setq eldoc-documentation-functions
	(cons #'flymake-eldoc-function
	      (remove #'flymake-eldoc-function eldoc-documentation-functions)))
  ;; Eglot optinally integrates with Markdown mode. This configues it only for
  ;; that purpose
  (with-eval-after-load 'markdown-mode
    (add-hook 'eldoc-mode-hook '+eglot-eldoc-setup-keys)
    (add-hook 'eldoc-mode-hook '+eglot-eldoc-disable-md-scaling)))

(setq eldoc-documentation-strategy #'eldoc-documentation-compose
      eldoc-echo-area-prefer-doc-buffer t
      eldoc-echo-area-use-multiline-p nil)

(add-hook 'eglot-managed-mode-hook '+eglot-eldoc)

(setopt eglot-stay-out-of '(flymake)
	eglot-autoshutdown t
	eglot-sync-connect 0
	eglot-extend-to-xref nil
	eglot-events-buffer-size 0
	eglot-ignored-server-capabilities
	'(:inlayHintProvider :documentFormattingProvider))

(defun +eglot-flymake ()
  (add-hook 'flymake-diagnostic-functions 'eglot-flymake-backend nil t))

(add-hook 'eglot-managed-mode-hook '+eglot-flymake)
(add-hook 'eglot-managed-mode-hook 'flymake-mode)

(with-eval-after-load 'eglot
  (set-face-attribute
   'eglot-mode-line nil
   :foreground nil
   :inherit '(font-lock-keyword-face)))

(add-hook 'compilation-filter-hook #'ansi-color-compilation-filter)

(unless (package-installed-p 'rainbow-mode)
  (package-install 'rainbow-mode))

(with-eval-after-load 'embark
  (push 'embark--allow-edit
	(alist-get 'eglot-rename embark-target-injection-hooks))

  (define-key embark-identifier-map (kbd "i") 'eglot-find-implementation)
  (define-key embark-identifier-map (kbd "t") 'eglot-find-typeDefinition)
  (define-key embark-identifier-map (kbd "d") 'eglot-find-declaration)
  (define-key embark-identifier-map (kbd "r") 'eglot-rename))

(unless (package-installed-p 'lsp-snippet)
  (package-vc-install "https://github.com/svaante/lsp-snippet"))

(with-eval-after-load 'eglot
  (lsp-snippet-tempel-eglot-init))

(unless (package-installed-p 'apheleia)
  (package-install 'apheleia))

(unless (package-installed-p 'dape)
  (package-install 'dape))

(setq dape-key-prefix (kbd "C-c C-a")
      dape-buffer-window-arrangement 'gud)

(provide '+ide)
;;; +ide.el ends here
