;;; +keybinds.el --- keybinding configuration    -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

(when (equal system-type 'darwin)
  (setopt default-input-method "MacOSX"
	  mac-command-modifier 'meta
	  mac-option-modifier nil
	  mac-allow-anti-aliasing t
	  mac-command-key-is-meta t))

(when (string-match "asahi" operating-system-release)
  ;; alt and win physical keys are swapped on M1 hardware
  (setopt x-super-keysym 'meta))

(provide '+keybinds)
;;; +keybinds.el ends here
