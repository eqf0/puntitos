;;; +chats.el --- instant messaging                  -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:
(unless (package-installed-p 'telega)
  (package-install 'telega))

(with-eval-after-load 'telega
  (define-key telega-msg-button-map (kbd "k") nil)
  (define-key telega-msg-button-map (kbd "SPC") nil)

  (setq telega-sticker-size '(8 . 24)
	telega-completing-read-function 'completing-read
	telega-use-images '(scale rotate90)
	telega-chat-input-markups '("org" "markdown2" nil)
	telega-emoji-use-images nil)

  (set-face-attribute
   'telega-entity-type-code nil
   :inherit 'font-lock-constant-face)
  (set-face-attribute
   'telega-entity-type-pre nil
   :inherit 'font-lock-constant-face))

(add-hook 'telega-root-mode-hook 'telega-notifications-mode)

(with-eval-after-load '+buffers
  (+yeq-add "Telega" 'telega))

(unless (package-installed-p 'ement)
  (package-install 'ement))

(setopt ement-save-sessions t
	ement-room-message-format-spec "%t %L%B%r"
	ement-room-shr-use-fonts t
	ement-notify-sound "message-new-instant"
	ement-notify-notification-predicates
	'(ement-notify--event-mentions-session-user-p
	  ement-notify--event-mentions-room-p
	  ement-notify--room-buffer-live-p)
	ement-room-left-margin-width 10
	ement-room-right-margin-width 10)

(add-hook 'ement-room-mode-hook 'mixed-pitch-mode)
(with-eval-after-load 'meow
  (add-to-list 'meow-mode-state-list '(ement-room-mode . motion)))

(defun +erc-connect (server)
  (erc-tls
   :server server
   :port 6697))

(defun +erc-connect-pine ()
  (interactive)
  (+erc-connect "irc.pine64.org"))

(defun +erc-connect-libera ()
  (interactive)
  (+erc-connect "irc.libera.chat"))

(defun +erc-connect-oftc ()
  (interactive)
  (+erc-connect "irc.oftc.net"))

(with-eval-after-load 'erc
  (add-hook 'erc-mode-hook 'visual-fill-column-mode)
  (setq erc-prompt-for-password nil
	erc-interpret-mirc-color t))

(provide '+chats)
;;; +chats.el ends here
