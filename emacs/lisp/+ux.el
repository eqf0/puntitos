;;; +ux.el --- look and feel                     -*- lexical-binding: t; -*-

;;; Code:

(unless (package-installed-p 'visual-fill-column)
  (package-install 'visual-fill-column))

(with-eval-after-load 'visual-fill-column
  (customize-set-variable 'visual-fill-column-center-text t)
  (customize-set-variable 'visual-fill-column-fringes-outside-margins nil)
  (customize-set-variable 'visual-fill-column-width 120))

(setq help-window-select t)

(unless (package-installed-p 'helpful)
  (package-install 'helpful))

(when (require 'helpful nil 'noerror)
  (define-key global-map (kbd "C-h f") 'helpful-callable)
  (define-key global-map (kbd "C-h v") 'helpful-variable)
  (define-key global-map (kbd "C-h k") 'helpful-key)
  (define-key global-map (kbd "C-h d") 'helpful-at-point))

(unless (package-installed-p 'debbugs)
  (package-install 'debbugs))

(provide '+ux)
;;; +ux.el ends here
