;;; +clang.el --- extra c/c++ configuration      -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

(with-eval-after-load 'treesit
  (add-to-list 'treesit-language-source-alist
	       '(c . ("https://github.com/tree-sitter/tree-sitter-c")))
  (add-to-list 'treesit-language-source-alist
	       '(cpp . ("https://github.com/tree-sitter/tree-sitter-cpp")))
  (add-to-list 'treesit-language-source-alist
	       '(cmake . ("https://github.com/uyha/tree-sitter-cmake"))))

(with-eval-after-load 'project
  (add-to-list 'project-vc-extra-root-markers "Makefile"))

(provide '+clang)
;;; +clang.el ends here
