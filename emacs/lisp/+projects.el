;;; +projects.el --- workspaces in emacs             -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

(with-eval-after-load 'project
  (setq project-switch-commands 'project-dired))

(defun +tab-bar-explicit-tabs (&optional frame)
  (seq-filter
   (lambda (tab) (alist-get 'explicit-name (cdr tab)))
   (tab-bar-tabs frame)))

(with-eval-after-load 'tab-bar
  (setq tab-bar-close-button-show nil
	tab-bar-new-tab-choice "*scratch*"
	tab-bar-format
	'(tab-bar-format-tabs tab-bar-format-separator tab-bar-format-add-tab)
	tab-bar-tabs-function 'tab-bar-tabs))

(provide '+projects)
;;; +projects.el ends here
