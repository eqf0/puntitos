;;; +java.el --- java extra workarounds          -*- lexical-binding: t; -*-

;;; Commentary:

;;; Code:

(unless (package-installed-p 'eglot-java)
  (package-install 'eglot-java))

(setopt eglot-java-eclipse-jdt-args
      (list
       (concat
	"-javaagent:"
	"/home/hochata/.gradle/caches/modules-2/files-2.1/org.projectlombok/lombok/1.18.30/f195ee86e6c896ea47a1d39defbe20eb59cd149d/lombok-1.18.30.jar")))

(unless (package-installed-p 'groovy-mode)
  (package-install 'groovy-mode))

(defun +eglot-jdtls (_interactive)
  (let ((cache
	 (expand-file-name (md5 (project-root (project-current t)))
			   (locate-user-emacs-file
			    "eglot-eclipse-jdt-cache"))))
		 `("jdtls" "-data" ,cache)))

;; (with-eval-after-load 'eglot
;;   (add-to-list 'eglot-server-programs '(java-ts-mode . +eglot-jdtls)))

(add-hook 'java-ts-mode-hook 'eglot-java-mode)

(with-eval-after-load 'treesit
  (add-to-list 'treesit-language-source-alist
	       '(java . ("https://github.com/tree-sitter/tree-sitter-java"))))

(add-to-list 'major-mode-remap-alist '(java-mode . java-ts-mode))

(with-eval-after-load 'project
  (add-to-list
   'project-vc-extra-root-markers
   "pom.xml"
   "build.gradle"))

(with-eval-after-load 'apheleia
  (setf
   (alist-get 'spotless apheleia-formatters)
   '("apheleia-from-project-root"
     "gradlew"
     "./gradlew"
     "--quiet"
     "spotlessApply"
     (concat "-PspotlessIdeHook=" (apheleia-formatters-local-buffer-file-name))
     "-PspotlessIdeHookUseStdIn"
     "-PspotlessIdeHookUseStdOut")
   (alist-get 'java-ts-mode apheleia-mode-alist) 'spotless))

(setopt java-ts-mode-indent-offset 2)

(add-hook 'java-ts-mode-hook (lambda () (indent-tabs-mode -1)))
(add-hook 'java-ts-mode-hook 'apheleia-mode-maybe)

(provide '+java)
;;; +java.el ends here
