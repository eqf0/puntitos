;;; dart-ts-mode.el --- dart mode based on treesitter  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Edgar Quiroz

;; Author: Edgar Quiroz <hochata@disroot.org>
;; Keywords: languages

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'treesit)

(defvar dart-ts-mode--keywords
  '("import" "library" "extension" "on" "class" "enum" "extends" "in"
    "is" "new" "return" "super" "with" "if" "else" "switch" "default" "try"
    "throw" "catch" "finally" "do" "while" "continue" "for")
  "Dart keywords.")

(defvar dart-ts-mode--builtins
  '("abstract" "as" "async" "async*" "yield" "sync*" "await" "covariant"
    "deferred" "dynamic" "external" "factory" "get" "implements" "interface"
    "library" "operator" "mixin" "part" "set" "show" "static" "typedef")
  "Dart builtin identifiers.")

(defvar dart-ts-mode--operators
  '("@" "=>" ".." "??" "==" "?" ":" "&&" "%" "<" ">" "=" ">=" "<=" "||")
  "Dart operators.")

(defvar dart-ts-mode--font-lock-settings
  (treesit-font-lock-rules
   :language 'dart
   :feature 'delimiter
   '([";" "." ","] @font-lock-delimiter-face)

   :language 'dart
   :feature 'bracket
   '(["(" ")" "[" "]" "{" "}"] @font-lock-bracket-face)

  :language 'dart
  :feature 'keyword
  `([,@dart-ts-mode--keywords (break_statement) (case_builtin)]
    @font-lock-keyword-face)

  :language 'dart
  :feature 'builtin
  `([,@dart-ts-mode--builtins (const_builtin) (final_builtin)]
    @font-lock-builtin-face)

  :language 'dart
  :feature 'operator
  `([,@dart-ts-mode--operators
     (increment_operator)
     (is_operator)
     (prefix_operator)
     (equality_operator)
     (additive_operator)
     ] @font-lock-operator-face)

  :language 'dart
  :feature 'comment
  '((comment) @font-lock-comment-face
    (documentation_comment) @font-lock-doc-face)

  :language 'dart
  :feature 'literal
  '([(true) (false) (null_literal) (symbol_literal)] @font-lock-constant-face
    [(hex_integer_literal)
     (decimal_integer_literal)
     (decimal_floating_point_literal)] @font-lock-number-face)

  :language 'dart
  :feature 'string
   '((string_literal) @font-lock-string-face)))

(define-derived-mode dart-ts-mode prog-mode "Dart"
  "Major mode for editing Dart files, powered by tree-sitter."
  (when (treesit-ready-p 'dart)
    (treesit-parser-create 'dart)

    ;; Comments
    (setq-local comment-start "//"
		comment-end "")

    ;; Indentation
    (setq-local indent-tabs-mode -1)

    ;; Font lock
    (setq-local treesit-font-lock-settings dart-ts-mode--font-lock-settings)
    (setq-local treesit-font-lock-feature-list
		'((bracket delimiter keyword builtin comment literal string) nil nil nil))
    (treesit-major-mode-setup)))

(provide 'dart-ts-mode)
;;; dart-ts-mode.el ends here
