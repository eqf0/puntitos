;;; zxcvm.el --- transient interface to asdf-vm     -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Edgar Quiroz

;; Author: Edgar Quiroz <hochata@disroot.org>
;; Keywords: tools, languages

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(require 'tabulated-list)
(require 'seq)

(defgroup zxcvm nil
  "Settings to interact with asdf."
  :version "0.1.0"
  :group 'applications)

(defun zxcvm--command (&rest args)
  "Build ASDF command using ARGS"
   (string-join (cons "asdf" args) " "))

(defun zxcvm--format-entries (raw-output format-entry-fn)
  "Format RAW-OUTPUT lines using FORMAT-ENTRY-FN.

FORMAT-ENTRY-FN should receive as an argument a list of strings."
  (let ((entry-lines (string-split raw-output "\n" t)))
    (seq-map
     (lambda (line)
       (let* ((parts (string-split line " " t)))
	 (funcall format-entry-fn parts)))
     entry-lines)))

(defun zxcvm--listing-command (buffer-name entries tab-mode)
  "Generic function to go along with a `tabulated-list-mode'
derived mode."
  (let* ((buffer (get-buffer-create buffer-name))
	 (tabbed-entries
	  (seq-map (lambda (entry) `(nil [,@entry])))))
    (pop-to-buffer buffer)
    (funcall tabulated-mode)
    (setq-local tabulated-list-entries tabbed-entries)
    (tabulated-list-print)))

(defvar-keymap zxcvm-current-list-mode-map
  :doc "Keymap for the current ZXCVM table"
  :parent tabulated-list-mode-map
  "m" #'zxcvm-mark-and-forward
  "d" #'zxcvm-flag-and-forward
  "u" #'zxcvm-unmark-and-forward)

(define-derived-mode
  zxcvm-current-list-mode
  tabulated-list-mode
  "ZXCV"
  "Display ASDF currently active installations"
  :interactive nil
  :group 'zxcvm
  (setq-local
   tabulated-list-format
   [("Plugin" 15 t)
    ("Version" 30 t)
    ("File" 1 t)
    ])
  (tabulated-list-init-header))

(defun zxcvm-current ()
  (zxcvm--format-entries
   (shell-command-to-string (zxcvm--command"current"))
   (lambda (entry)
     (seq-let (name version &rest locations) entry
       (list name version (string-join locations " "))))))

(defun zxcvm-show-current ()
  "Show currently active ASDF installs"
  (interactive)
  (zxcvm--listing-command
   "*ZXCVM Current*"
   (zxcvm-current)
   'zxcvm-current-list-mode))

(defvar-keymap zxcvm-plugin-list-mode-map
  :doc "Keymap for the ZXCVM plugin list table"
  :parent tabulated-list-mode-map
  "m" #'zxcvm-mark-and-forward
  "d" #'zxcvm-flag-and-forward
  "u" #'zxcvm-unmark-and-forward)

(define-derived-mode
  zxcvm-plugin-list-mode
  tabulated-list-mode
  "ZXCVM Plugins"
  "Display ASDF currently installed plugins"
  :interactive nil
  :group 'zxcvm
  (setq-local
   tabulated-list-format
   [("Plugin" 15 t)
    ("URL" 60 t)
    ("Branch" 15 t)
    ("Commit" 1 t)
    ])
  (tabulated-list-init-header))

(defun zxcvm-plugin-list ()
  (zxcvm--format-entries
   (shell-command-to-string
    (zxcvm--command "plugin" "list" "--urls" "--refs"))
   (lambda (entry)
     (seq-let (name url branch commit) entry
       (list name url branch commit)))))

(defun zxcvm-show-plugin-list ()
  "Show currently active ASDF installs"
  (interactive)
  (zxcvm--listing-command
   "*ZXCVM Plugins*"
   (zxcvm-plugin-list)
   'zxcvm-plugin-list-mode))

(defun zxcvm-plugin-select-new ()
  (let*
      ((entries-alist
	(zxcvm--format-entries
	 (shell-command-to-string
	  (zxcvm--command "plugin" "list" "all"))
	 (lambda (entry)
	   (seq-let (key value) entry (cons key value)))))
       (completion-extra-properties
	`(:annotation-function
	  ,(lambda (entry)
	     (string-join
	      `("\t"
		,(alist-get entry
			    entries-alist
			    nil
			    nil
			    'equal)))))))
    (completing-read
     "Select plugin: "
     entries-alist
     (lambda (cell) (not (string-prefix-p "*" (cdr cell))))
     nil)))

(defun zxcvm--act-at-point (act-fn &rest args)
  "Execute ACT-FN with the beg and end from line at point."
  (let* ((beg (line-beginning-position))
	 (end (line-end-position)))
    (apply act-fn beg end args)))

(defun zxcvm--set-prop-action (beg end face prop)
  (let ((overlay (make-overlay beg end))
	(inhibit-read-only t))
    (overlay-put overlay 'face face)
    (put-text-property beg end prop ?>)))

(defface zxcvm-marked-face '((t (:inherit dired-marked)))
  "Face for marked items in ZXCVM modes"
  :group 'zxcvm)

(defun zxcvm--remove-props-action (beg end marks)
  (let ((inhibit-read-only t))
    (remove-overlays beg end)
    (seq-map
     (lambda (mark)
       (remove-text-properties beg end `(,mark nil))))))

(defun zxcvm-mark-and-forward ()
  (interactive)
  (zxcvm--act-at-point
   #'zxcvm--remove-props-action
   '(zxcvm-flag))
  (zxcvm--act-at-point
   #'zxcvm--set-prop-action
   'zxcvm-mark
   'zxcvm-marked-face
   'zxcvm-mark)
  (forward-line))

(defun zxcvm-unmark-and-forward ()
  (interactive)
  (zxcvm--act-at-point
   #'zxcvm--remove-prop-action
   '(zxcvm-mark zxcvm-flag))
  (forward-line))

(defface zxcvm-flagged-face '((t (:inherit dired-flagged)))
  "Face for items flagged for deletion in ZXCVM modes"
  :group 'zxcvm)

(defun zxcvm-flag-and-forward ()
  (interactive)
  (zxcvm--act-at-point
   #'zxcvm--remove-props-action
   '(zxcvm-mark))
  (zxcvm--act-at-point
   #'zxcvm--set-prop-action
   'zxcvm-flag
   'zxcvm-flaged-face
   'zxcvm-flag)
  (forward-line))

(provide 'zxcvm)
;;; zxcvm.el ends here
